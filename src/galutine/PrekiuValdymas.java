package galutine;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class PrekiuValdymas {

  public static void showList(List<Preke> prekes) {
    for (int i = 0; i < prekes.size(); i++) {
      System.out.println(prekes.get(i));
    }
  }

  public static void irasyti(List<Preke> prekes, String failoKelias) {
    try (BufferedWriter irasyti = new BufferedWriter(new FileWriter(failoKelias))) {
      for (int i = 0; i < prekes.size(); i++) {
        irasyti.write(prekes.get(i).toString() + "\n");
        System.out.println(prekes.get(i));
      }
    } catch (FileNotFoundException e) {
      System.out.println("Įrašymo kelias nerastas");
    } catch (Exception e) {
      System.out.println("Klaida irasant" + e);
    }
  }

  //      1 veiksmas: Rasti prekę, kurios vienetų turi mažiausiai ir ją atspausdinti konsolėje;
  public static Preke maziausiaiPrekiu(List<Preke> prekes) {
    if (prekes.size() > 0) {
      Integer maziausiasKiekis = prekes.get(0).getKiekis();
      Integer index = 0;
      for (int i = 0; i < prekes.size(); i++) {
        if (maziausiasKiekis > prekes.get(i).getKiekis()) {
          maziausiasKiekis = prekes.get(i).getKiekis();
          index = i;
        }
      }
      return prekes.get(index);
    }
    return null;
  }

  //      2 veiksmas: Rasti prekę, kurios vienetų turi daugiausiai ir ją atspausdinti konsolėje;
  public static Preke daugiausiaiPrekiu(List<Preke> prekes) {
    if (prekes.size() > 0) {
      Integer didziausiasKiekis = prekes.get(0).getKiekis();
      Integer index = 0;
      for (int i = 0; i < prekes.size(); i++) {
        if (didziausiasKiekis < prekes.get(i).getKiekis()) {
          didziausiasKiekis = prekes.get(i).getKiekis();
          index = i;
        }
      }
      return prekes.get(index);
    }
    return null;
  }

  //      3 veiksmas: Išfiltruoti prekes pagal tipą ir atspausdinti į failą
  // atfiltruotosPrekes.txt;
  public static void spausdisntiPagalTipa(List<Preke> prekes, Scanner skaitytuvas) {
    String pasirinktasTipas = pasirinkiteTipa(skaitytuvas);
    String failoKelias = new File("").getAbsolutePath() + "/src/galutine/atfiltruotosPrekes.txt";
    List<Preke> atrinktos = new ArrayList<>();
    for (int i = 0; i < prekes.size(); i++) {
      String prekesTipas = prekes.get(i).getTipas().getTipoPavadinimas();
      if (prekesTipas.equals(pasirinktasTipas)) {
        atrinktos.add(prekes.get(i));
      }
    }
    irasyti(atrinktos, failoKelias);
  }

  //      4 veiksmas: Pridėti prekę prie esančio sąrašo ir tą sąrašą išsaugoti prekes.txt faile;
  public static void pridetiPreke(List<Preke> prekes, Preke preke, String failoKelias) {
    prekes.add(preke);
    irasyti(prekes, failoKelias);
  }

  //      5 veiksmas: Ištrinti prekę pagal prekės id ir tą sąrašą išsaugoti prekes.txt faile;
  public static void istrintiPreke(List<Preke> prekes, Integer id, String failoKelias) {
    Integer index = -1;
    for (int i = 0; i < prekes.size(); i++) {
      if (prekes.get(i).getId().equals(id)) {
        index = i;
        break;
      }
    }
    if (index >= 0) {
      prekes.remove(prekes.get(index));
      irasyti(prekes, failoKelias);
    } else {
      System.out.println("Tokios prekės nėra");
    }
  }

  //      6 veiksmas: Redaguoti prekę pagal prekės id ir tą sąrašą išsaugoti prekes.txt faile
  public static void redaguotiPreke(
          List<Preke> prekes, Integer id, Preke preke, String failoKelias) {
    for (int i = 0; i < prekes.size(); i++) {
      if (prekes.get(i).getId().equals(id)) {
        prekes.set(i, preke);
        break;
      }
    }
    irasyti(prekes, failoKelias);
  }

  public static String pasirinkiteTipa(Scanner skaitytuvas) {
    Integer tipas =
        ivestiSkaiciu(
            "Pasireinkite prekės tipą Televizoriai [1], Nesiojamas kompiuteris [2], Skalbykle [3],  Dziovykle [4]: ",
            skaitytuvas);
    String tipoPavadinimas;
    switch (tipas) {
      case 1:
        {
          tipoPavadinimas = "televizoriai";
          break;
        }
      case 2:
        {
          tipoPavadinimas = "nesiojamas-kompiuteris";
          break;
        }
      case 3:
        {
          tipoPavadinimas = "skalbykle";
          break;
        }
      case 4:
        {
          tipoPavadinimas = "dziovykle";
          break;
        }
      default:
        {
          pasirinkiteTipa(skaitytuvas);
          tipoPavadinimas = "";
        }
    }
    return tipoPavadinimas;
  }

  public static Preke prekesMeniu(Integer prekesId, Scanner skaitytuvas) {
    Boolean testi = true;
    Tipas techtipas = null;
    while (testi) {
      String prekesTipas = pasirinkiteTipa(skaitytuvas);
      switch (prekesTipas) {
        case "televizoriai":
          {
            System.out.print("Įveskite televizoriaus technologija, raiska: ");
            String technologija = skaitytuvas.next();
            String raiska = skaitytuvas.next();
            techtipas = new Televizorius(technologija, raiska);
            testi = false;
          }
          break;
        case "nesiojamas-kompiuteris":
          {
            System.out.print("Įveskite nešiojamo kompiuterio procesoriu ram ir disko talpa: ");
            String procesorius = skaitytuvas.next();
            String ram = skaitytuvas.next();
            String diskoTalpa = skaitytuvas.next();
            techtipas = new Laptopas(procesorius, ram, diskoTalpa);
            testi = false;
          }
          break;
        case "skalbykle":
          {
            System.out.print("Įveskite skalbykles talpą: ");
            String talposKiekis = skaitytuvas.next();
            techtipas = new Skalbykle(talposKiekis);
            testi = false;
          }
          break;
        case "dziovykle":
          {
            System.out.print("Įveskite dziovykles talpą: ");
            String talposKiekis = skaitytuvas.next();
            techtipas = new Dziovykle(talposKiekis);
            testi = false;
          }
          break;
        default:
          {
            System.out.println("Nera tokio tipo prekiu");
          }
      }
    }
    System.out.print("Įveskite pavadinimą: ");
    String pavadinimas = skaitytuvas.next();
    Integer kiekis = ivestiSkaiciu("Įveskite kiekį: ", skaitytuvas);
    Double kaina = ivestiDouble("Įveskite vnt. kainą: ", skaitytuvas);
    Preke preke = new Preke(prekesId, techtipas, pavadinimas, kiekis, kaina);
    return preke;
  }

  public static void pridetiPrekeMeniu(
          List<Preke> prekes, Scanner skaitytuvas, String failoKelias) {
    Integer id = 1;
    if (prekes.size() != 0) {
      id = prekes.get(prekes.size() - 1).getId() + 1;
    }
    Preke preke = prekesMeniu(id, skaitytuvas);
    pridetiPreke(prekes, preke, failoKelias);
  }

  public static void redagavimoMeniu(List<Preke> prekes, Scanner skaitytuvas, String failoKelias) {
    showList(prekes);
    Integer prekesId = ivestiSkaiciu("Pasirinkite prekės kuria norite redaguoti id: ", skaitytuvas);
    Integer index = -1;

    for (int i = 0; i < prekes.size(); i++) {
      if (prekes.get(i).getId().equals(prekesId)) {
        index = i;
      }
    }
    if (index >= 0) {
      Preke preke = prekesMeniu(prekesId, skaitytuvas);
      redaguotiPreke(prekes, prekesId, preke, failoKelias);
    } else {
      System.out.print("Tokios prekės nėra pasirinkite kitą: ");
      redagavimoMeniu(prekes, skaitytuvas, failoKelias);
    }
  }

  public static Integer ivestiSkaiciu(String zinute, Scanner scanner) {
    System.out.print(zinute);
    Integer skaicius;
    try {
      skaicius = scanner.nextInt();
    } catch (InputMismatchException e) {
      scanner.next();
      skaicius = ivestiSkaiciu(zinute, scanner);
    }
    return skaicius;
  }

  public static Double ivestiDouble(String zinute, Scanner scanner) {
    System.out.print(zinute);
    Double skaicius;
    try {
      skaicius = scanner.nextDouble();
    } catch (Exception e) {
      scanner.next();
      skaicius = ivestiDouble(zinute, scanner);
    }
    return skaicius;
  }
}
