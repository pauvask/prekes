package galutineSuMap;

public class Preke {
  private Integer id;
  private Tipas tipas;
  private String pavadinimas;
  private Integer kiekis;
  private Double kaina;

  public Preke(Integer id, Tipas tipas, String pavadinimas, Integer kiekis, Double kaina) {
    this.id = id;
    this.tipas = tipas;
    this.pavadinimas = pavadinimas;
    this.kiekis = kiekis;
    this.kaina = kaina;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Tipas getTipas() {
    return tipas;
  }

  public void setTipas(Tipas tipas) {
    this.tipas = tipas;
  }

  public String getPavadinimas() {
    return pavadinimas;
  }

  public void setPavadinimas(String pavadinimas) {
    this.pavadinimas = pavadinimas;
  }

  public Integer getKiekis() {
    return kiekis;
  }

  public void setKiekis(Integer kiekis) {
    this.kiekis = kiekis;
  }

  public Double getKaina() {
    return kaina;
  }

  public void setKaina(Double kaina) {
    this.kaina = kaina;
  }

  @Override
  public String toString() {
    return id
        + " "
        + tipas.getTipoPavadinimas()
        + " "
        + pavadinimas
        + " "
        + kiekis
        + " "
        + kaina
        + " "
        + tipas;
  }
}
