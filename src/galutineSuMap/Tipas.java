package galutineSuMap;

public abstract class Tipas {

  public abstract String getTipoPavadinimas();

  @Override
  public abstract String toString();
}
