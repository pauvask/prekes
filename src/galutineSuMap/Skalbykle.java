package galutineSuMap;

public class Skalbykle extends Tipas {
  private String talposKiekis;

  public Skalbykle(String talposKiekis) {
    this.talposKiekis = talposKiekis;
  }

  @Override
  public String getTipoPavadinimas() {
    return "skalbykle";
  }

  @Override
  public String toString() {
    return talposKiekis;
  }
}
