package galutineSuMap;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class GalutiniosuMapMain {
  public static void main(String[] args) {
    Scanner skaitytuvas = new Scanner(System.in);
    drawMeniu(skaitytuvas);
    skaitytuvas.close();
  }

  private static void skaityti(String failokelias, Map<Integer, Preke> prekes) {
    try (BufferedReader skaitytuvas = new BufferedReader(new FileReader(failokelias))) {
      String eilute = skaitytuvas.readLine();
      while (eilute != null) {
        String[] reiksmes = eilute.split(" ");
        Integer id = Integer.parseInt(reiksmes[0]);
        String tipas = reiksmes[1];
        String pavadinimas = reiksmes[2];
        Integer kiekis = Integer.parseInt(reiksmes[3]);
        Double kaina = Double.parseDouble(reiksmes[4]);
        Tipas techtipas =
            new Tipas() {
              @Override
              public String getTipoPavadinimas() {
                return null;
              }

              @Override
              public String toString() {
                return null;
              }
            };
        switch (tipas) {
          case "televizoriai":
            {
              String technologija = reiksmes[5];
              String raiska = reiksmes[6];
              techtipas = new Televizorius(technologija, raiska);
            }
            break;
          case "nesiojamas-kompiuteris":
            {
              String procesorius = reiksmes[5];
              String ram = reiksmes[6];
              String diskoTalpa = reiksmes[7];
              techtipas = new Laptopas(procesorius, ram, diskoTalpa);
            }
            break;
          case "skalbykle":
            {
              String talposKiekis = reiksmes[5];
              techtipas = new Skalbykle(talposKiekis);
            }
            break;
          case "dziovykle":
            {
              String talposKiekis = reiksmes[5];
              techtipas = new Dziovykle(talposKiekis);
            }
            break;
          default:
            {
              System.out.println("Nera tokio tipo prekiu");
            }
        }
        Preke preke = new Preke(id, techtipas, pavadinimas, kiekis, kaina);
        prekes.put(id, preke);
        eilute = skaitytuvas.readLine();
      }
    } catch (FileNotFoundException e) {
      System.out.println("Skaitymo kelias nerastas");
    } catch (Exception e) {
      System.out.println("Nepavyko nuskaityti " + e);
    }
  }

  private static Boolean yraPrekiu(Map<Integer, Preke> prekes) {
    if (prekes.size() == 0) {
      System.out.println("Nėra prekių");
      return false;
    } else {
      return true;
    }
  }

  private static void drawMeniu(Scanner skaitytuvas) {
    System.out.println(
        "\n\n1 veiksmas: Rasti prekę, kurios vienetų turi mažiausiai ir ją atspausdinti konsolėje;\n"
            + "2 veiksmas: Rasti prekę, kurios vienetų turi daugiausiai ir ją atspausdinti konsolėje;\n"
            + "3 veiksmas: Išfiltruoti prekes pagal tipą ir atspausdinti į failą atfiltruotosPrekes.txt;\n"
            + "4 veiksmas: Pridėti prekę prie esančio sąrašo ir tą sąrašą išsaugoti prekes.txt faile;\n"
            + "5 veiksmas: Ištrinti prekę pagal prekės id ir tą sąrašą išsaugoti prekes.txt faile;\n"
            + "6 veiksmas: Redaguoti prekę pagal prekės id ir tą sąrašą išsaugoti prekes.txt faile\n");
    System.out.print("Įveskite veiksmo numerį [1-6]: ");
    String failoKelias = new File("").getAbsolutePath() + "/src/galutineSuMap/prekes.txt";
    Map<Integer, Preke> prekes = new HashMap<>();
    skaityti(failoKelias, prekes);
    while (!skaitytuvas.hasNextInt()) {
      System.out.print("Įveskite veiksmo numerį [1-6]: ");
      skaitytuvas.next();
    }
    int skaicius = skaitytuvas.nextInt();
    switch (skaicius) {
      case 1:
        {
          if (yraPrekiu(prekes))
            System.out.println("Mažiausiai prekių: " + PrekiuValdymas.maziausiaiPrekiu(prekes));
        }
        break;
      case 2:
        {
          if (yraPrekiu(prekes))
            System.out.println("Daugiausiai prekiu " + PrekiuValdymas.daugiausiaiPrekiu(prekes));
        }
        break;
      case 3:
        {
          if (yraPrekiu(prekes)) PrekiuValdymas.spausdisntiPagalTipa(prekes, skaitytuvas);
        }
        break;
      case 4:
        {
          PrekiuValdymas.pridetiPrekeMeniu(prekes, skaitytuvas, failoKelias);
          PrekiuValdymas.showList(prekes);
        }
        break;
      case 5:
        {
          if (yraPrekiu(prekes)) {
            PrekiuValdymas.showList(prekes);
            System.out.print("Pasirinkite prekės id, kurios norite ištrinti: ");
            while (!skaitytuvas.hasNextInt()) {
              System.out.println("Įveskite skaičių");
              skaitytuvas.next();
            }
            Integer prekesId = skaitytuvas.nextInt();
            PrekiuValdymas.istrintiPreke(prekes, prekesId, failoKelias);
          }
        }
        break;
      case 6:
        {
          if (yraPrekiu(prekes)) {
            PrekiuValdymas.redagavimoMeniu(prekes, skaitytuvas, failoKelias);
          }
        }
        break;
      default:
        {
        }
    }
    drawMeniu(skaitytuvas);
  }
}
