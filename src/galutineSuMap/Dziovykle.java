package galutineSuMap;

public class Dziovykle extends Tipas {
  private String talposKiekis;

  public Dziovykle(String talposKiekis) {
    this.talposKiekis = talposKiekis;
  }

  @Override
  public String getTipoPavadinimas() {
    return "dziovykle";
  }

  public String getTalposKiekis() {
    return talposKiekis;
  }

  public void setTalposKiekis(String talposKiekis) {
    this.talposKiekis = talposKiekis;
  }

  @Override
  public String toString() {
    return talposKiekis;
  }
}
