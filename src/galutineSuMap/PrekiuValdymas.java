package galutineSuMap;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Map;
import java.util.Scanner;

public class PrekiuValdymas {

  public static void showList(Map<Integer, Preke> prekes) {
    for (Preke preke : prekes.values()) {
      System.out.println(preke);
    }
  }

  public static void irasyti(Map<Integer, Preke> prekes, String failoKelias) {
    try (BufferedWriter irasyti = new BufferedWriter(new FileWriter(failoKelias))) {
      for (Preke preke : prekes.values()) {
        irasyti.write(preke.toString() + "\n");
        System.out.println(preke);
      }
    } catch (FileNotFoundException e) {
      System.out.println("Įrašymo kelias nerastas");
    } catch (Exception e) {
      System.out.println("Klaida irasant" + e);
    }
  }

  //      1 veiksmas: Rasti prekę, kurios vienetų turi mažiausiai ir ją atspausdinti konsolėje;
  public static Preke maziausiaiPrekiu(Map<Integer, Preke> prekes) {
    if (prekes.size() > 0) {
      Integer maziausiasKiekis = Integer.MAX_VALUE;
      Integer id = 0;
      for (Preke preke : prekes.values()) {
        if (maziausiasKiekis > preke.getKiekis()) {
          maziausiasKiekis = preke.getKiekis();
          id = preke.getId();
        }
      }
      return prekes.get(id);
    }
    return null;
  }

  //      2 veiksmas: Rasti prekę, kurios vienetų turi daugiausiai ir ją atspausdinti konsolėje;
  public static Preke daugiausiaiPrekiu(Map<Integer, Preke> prekes) {
    if (prekes.size() > 0) {
      Integer didziausiasKiekis = Integer.MIN_VALUE;
      Integer id = 0;
      for (Preke preke : prekes.values()) {
        if (didziausiasKiekis < preke.getKiekis()) {
          didziausiasKiekis = preke.getKiekis();
          id = preke.getId();
        }
      }
      return prekes.get(id);
    }
    return null;
  }

  //      3 veiksmas: Išfiltruoti prekes pagal tipą ir atspausdinti į failą
  // atfiltruotosPrekes.txt;
  public static void spausdisntiPagalTipa(Map<Integer, Preke> prekes, Scanner skaitytuvas) {
    String pasirinktasTipas = pasirinkiteTipa(skaitytuvas);
    String failoKelias =
        new File("").getAbsolutePath() + "/src/galutineSuMap/atfiltruotosPrekes.txt";
    Map<Integer, Preke> atrinktos = new HashMap();
    for (Preke preke : prekes.values()) {
      String prekesTipas = preke.getTipas().getTipoPavadinimas();
      if (prekesTipas.equals(pasirinktasTipas)) {
        Integer id = preke.getId();
        Preke atrinktapreke = prekes.get(id);
        atrinktos.put(id, atrinktapreke);
      }
    }
    irasyti(atrinktos, failoKelias);
  }

  //      4 veiksmas: Pridėti prekę prie esančio sąrašo ir tą sąrašą išsaugoti prekes.txt faile;
  public static void pridetiPreke(Map<Integer, Preke> prekes, Preke preke, String failoKelias) {
    prekes.put(preke.getId(), preke);
    irasyti(prekes, failoKelias);
  }

  //      5 veiksmas: Ištrinti prekę pagal prekės id ir tą sąrašą išsaugoti prekes.txt faile;
  public static void istrintiPreke(Map<Integer, Preke> prekes, Integer id, String failoKelias) {
    if (prekes.containsKey(id)) {
      prekes.remove(id);
      irasyti(prekes, failoKelias);
    } else {
      System.out.println("Tokios prekės nėra");
    }
  }

  //      6 veiksmas: Redaguoti prekę pagal prekės id ir tą sąrašą išsaugoti prekes.txt faile
  public static void redaguotiPreke(
          Map<Integer, Preke> prekes, Integer id, Preke preke, String failoKelias) {
    if (prekes.containsKey(id)) {
      prekes.put(id, preke);
    }
    irasyti(prekes, failoKelias);
  }

  public static String pasirinkiteTipa(Scanner skaitytuvas) {
    Integer tipas =
        ivestiSkaiciu(
            "Pasireinkite prekės tipą Televizoriai [1], Nesiojamas kompiuteris [2], Skalbykle [3],  Dziovykle [4]: ",
            skaitytuvas);
    String tipoPavadinimas;
    switch (tipas) {
      case 1:
        {
          tipoPavadinimas = "televizoriai";
          break;
        }
      case 2:
        {
          tipoPavadinimas = "nesiojamas-kompiuteris";
          break;
        }
      case 3:
        {
          tipoPavadinimas = "skalbykle";
          break;
        }
      case 4:
        {
          tipoPavadinimas = "dziovykle";
          break;
        }
      default:
        {
          pasirinkiteTipa(skaitytuvas);
          tipoPavadinimas = "";
        }
    }
    return tipoPavadinimas;
  }

  public static Preke prekesMeniu(Integer prekesId, Scanner skaitytuvas) {
    Boolean testi = true;
    Tipas techtipas = null;
    while (testi) {
      String prekesTipas = pasirinkiteTipa(skaitytuvas);
      switch (prekesTipas) {
        case "televizoriai":
          {
            System.out.print("Įveskite televizoriaus technologija, raiska: ");
            String technologija = skaitytuvas.next();
            String raiska = skaitytuvas.next();
            techtipas = new Televizorius(technologija, raiska);
            testi = false;
          }
          break;
        case "nesiojamas-kompiuteris":
          {
            System.out.print("Įveskite nešiojamo kompiuterio procesoriu ram ir disko talpa: ");
            String procesorius = skaitytuvas.next();
            String ram = skaitytuvas.next();
            String diskoTalpa = skaitytuvas.next();
            techtipas = new Laptopas(procesorius, ram, diskoTalpa);
            testi = false;
          }
          break;
        case "skalbykle":
          {
            System.out.print("Įveskite skalbykles talpą: ");
            String talposKiekis = skaitytuvas.next();
            techtipas = new Skalbykle(talposKiekis);
            testi = false;
          }
          break;
        case "dziovykle":
          {
            System.out.print("Įveskite dziovykles talpą: ");
            String talposKiekis = skaitytuvas.next();
            techtipas = new Dziovykle(talposKiekis);
            testi = false;
          }
          break;
        default:
          {
            System.out.println("Nera tokio tipo prekiu");
          }
      }
    }
    System.out.print("Įveskite pavadinimą: ");
    String pavadinimas = skaitytuvas.next();
    Integer kiekis = ivestiSkaiciu("Įveskite kiekį: ", skaitytuvas);
    Double kaina = ivestiDouble("Įveskite vnt. kainą: ", skaitytuvas);
    Preke preke = new Preke(prekesId, techtipas, pavadinimas, kiekis, kaina);
    return preke;
  }

  private static Integer getMaxKey(Map<Integer, Preke> prekes) {
    Integer maxKey = null;
    int maxValue = Integer.MIN_VALUE;
    for (Integer key : prekes.keySet()) {
      if (maxValue < key) {
        maxValue = key;
        maxKey = key;
      }
    }
    return maxKey;
  }

  public static void pridetiPrekeMeniu(
          Map<Integer, Preke> prekes, Scanner skaitytuvas, String failoKelias) {
    Integer nextId = getMaxKey(prekes) + 1;
    Preke preke = prekesMeniu(nextId, skaitytuvas);
    pridetiPreke(prekes, preke, failoKelias);
  }

  public static void redagavimoMeniu(
          Map<Integer, Preke> prekes, Scanner skaitytuvas, String failoKelias) {
    showList(prekes);
    Integer prekesId = ivestiSkaiciu("Pasirinkite prekės kuria norite redaguoti id: ", skaitytuvas);
    if (prekes.containsKey(prekesId)) {
      Preke preke = prekesMeniu(prekesId, skaitytuvas);
      redaguotiPreke(prekes, prekesId, preke, failoKelias);
    } else {
      System.out.print("Tokios prekės nėra pasirinkite kitą: ");
      redagavimoMeniu(prekes, skaitytuvas, failoKelias);
    }
  }

  public static Integer ivestiSkaiciu(String zinute, Scanner scanner) {
    System.out.print(zinute);
    Integer skaicius;
    try {
      skaicius = scanner.nextInt();
    } catch (InputMismatchException e) {
      scanner.next();
      skaicius = ivestiSkaiciu(zinute, scanner);
    }
    return skaicius;
  }

  public static Double ivestiDouble(String zinute, Scanner scanner) {
    System.out.print(zinute);
    Double skaicius;
    try {
      skaicius = scanner.nextDouble();
    } catch (Exception e) {
      scanner.next();
      skaicius = ivestiDouble(zinute, scanner);
    }
    return skaicius;
  }
}
