package galutineSuMap;

public class Laptopas extends Tipas {
  private String procesorius;
  private String ram;
  private String diskoTalpa;

  public Laptopas(String procesorius, String ram, String diskoTalpa) {
    this.procesorius = procesorius;
    this.ram = ram;
    this.diskoTalpa = diskoTalpa;
  }

  public String getProcesorius() {
    return procesorius;
  }

  public void setProcesorius(String procesorius) {
    this.procesorius = procesorius;
  }

  public String getRam() {
    return ram;
  }

  public void setRam(String ram) {
    this.ram = ram;
  }

  public String getDiskoTalpa() {
    return diskoTalpa;
  }

  public void setDiskoTalpa(String diskoTalpa) {
    this.diskoTalpa = diskoTalpa;
  }

  @Override
  public String getTipoPavadinimas() {
    return "nesiojamas-kompiuteris";
  }

  @Override
  public String toString() {
    return procesorius + " " + ram + " " + diskoTalpa;
  }
}
