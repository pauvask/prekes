package galutineSuMap;

public class Televizorius extends Tipas {
  private String technologija;
  private String raiska;

  public Televizorius() {}

  public Televizorius(String technologija, String raiska) {
    this.technologija = technologija;
    this.raiska = raiska;
  }

  public String getTechnologija() {
    return technologija;
  }

  public void setTechnologija(String technologija) {
    this.technologija = technologija;
  }

  public String getRaiska() {
    return raiska;
  }

  public void setRaiska(String raiska) {
    this.raiska = raiska;
  }

  @Override
  public String getTipoPavadinimas() {
    return "televizoriai";
  }

  @Override
  public String toString() {
    return technologija + " " + raiska;
  }
}
